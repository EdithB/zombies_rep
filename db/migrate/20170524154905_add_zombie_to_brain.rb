class AddZombieToBrain < ActiveRecord::Migration[5.0]
  def change
    add_reference :brains, :Zombie, foreign_key: true
  end
end
